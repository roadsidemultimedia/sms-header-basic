/**
 * RS-BASIC.js
 * http://roadsidemultimedia.com/
 * Author: Curtis Grant
 */
    jQuery(function($) {
      $(window).ready(function() { 
          $('.rsnav').meanmenu();          
          var example = $('.rs-menu').superfish();     
              $(window).scroll(function(){
                if($(document).scrollTop() > 150)
                {
                    $(".navholder").addClass("fixed-nav");
                }
                else {
                    $(".navholder").removeClass("fixed-nav");
                }
              });
      });
      $( window ).resize(function() {
        if ($(window).width() < 1080) {
          $(".navholder").removeClass('fixed-nav');
        }
      });
    });